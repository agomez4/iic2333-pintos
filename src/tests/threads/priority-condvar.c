/* Tests timer_sleep(0), which should return immediately. */

#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"


static thread_func simp;
static thread_func cambio_prioridad;
static thread_func bloqueo;
static thread_func block_thread;
static int64_t wake_time;
static struct semaphore wait_sema;
struct lock candado;


void
test_priority_condvar (void) 
{
    
  lock_init(&candado);
  printf("se crean 3 threads con prioridad 40, 2 threads con prioridad 50 y uno con prioridad 20\n");
  timer_sleep(100);
  thread_create("flareon", 40, simp, NULL);
  thread_create("vaporeon", 40, simp, NULL);
  thread_create("jolteon", 40, simp, NULL);
  thread_create("espeon", 50, simp, NULL);
  thread_create("umbreon", 50, simp, NULL);
  thread_create("eevee", 20, simp, NULL);
  
  timer_sleep (1500);
  pass ();

}

static void
simp (void *aux UNUSED)
{
    
    int64_t i;
    for (i=0; i<69999999; i++){
    
        
    }
}

static void
cambio_prioridad(void *aux UNUSED){
  int64_t i;
    for (i=0; i<19999999; i++){
    
        
    }
  thread_set_priority(1);
  for (i=0; i<59999999; i++){
    
        
    }
}




static void
block_thread (void *aux UNUSED) 
{
  

  msg ("Block thread acquiring lock...");
  lock_acquire (&candado);

  msg ("...got it.");

  int64_t i;
  
    for (i=0; i<69999999; i++){
    
        
    }
    lock_release(&candado);
}
