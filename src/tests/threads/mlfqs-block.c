/* Tests timer_sleep(0), which should return immediately. */

#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"


static thread_func simp;
static thread_func cambio_prioridad;
static thread_func bloqueo;
static thread_func block_thread;
static int64_t wake_time;
static struct semaphore wait_sema;
struct lock candado;


void
test_mlfqs_block (void) 
{
    
  lock_init(&candado);

  printf("se crean 6 threads que se bloquean entre ellos \n");
  timer_sleep(100);
  thread_create("articuno", 10, block_thread, NULL);
  thread_create("zapdos", 20, block_thread, NULL);
  thread_create("moltres", 30, block_thread, NULL);
  thread_create("suicune", 40, block_thread, NULL);
  thread_create("raikou", 50, block_thread, NULL);
  thread_create("entei", 60, block_thread, NULL);
  
  timer_sleep (1000);
  pass ();

}

static void
simp (void *aux UNUSED)
{
    
    int64_t i;
    for (i=0; i<69999999; i++){
    
        
    }
}

static void
cambio_prioridad(void *aux UNUSED){
  int64_t i;
    for (i=0; i<19999999; i++){
    
        
    }
  thread_set_priority(1);
  for (i=0; i<59999999; i++){
    
        
    }
}




static void
block_thread (void *aux UNUSED) 
{
  

  msg ("Block thread acquiring lock...");
  lock_acquire (&candado);

  msg ("%s ...got it.", thread_name());

  int64_t i;
  
    for (i=0; i<69999999; i++){
    
        
    }
    lock_release(&candado);
}

