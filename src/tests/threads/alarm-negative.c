#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"


static thread_func simple;
static thread_func cambio_prioridad;
static thread_func bloqueo;
static thread_func block_thread;
static int64_t wake_time;
static struct semaphore wait_sema;
struct lock candado;


void
test_alarm_negative (void) 
{
    
  lock_init(&candado);
  thread_create("gengar", 40, simple, NULL);
  thread_create("machamp", 40, simple, NULL);
  thread_create("golem", 40, simple, NULL);
  thread_create("alakazam", 5, block_thread, NULL);
  thread_create("electabuzz", 12, block_thread, NULL);
  thread_create("lapras", 17, simple, NULL);
  thread_create("poliwrath", 60, cambio_prioridad, NULL);
  thread_create("arcanine", 19, cambio_prioridad, NULL);
  thread_create("ratatta", 33, simple, NULL);
  thread_create("growlithe", 62, simple, NULL);
  thread_create("bellsprout", 1, block_thread, NULL);
  
  timer_sleep (1000);
  pass ();

}

static void
simple (void *aux UNUSED)
{
    
    int64_t i;
    for (i=0; i<69999999; i++){
    
        
    }
}

static void
cambio_prioridad(void *aux UNUSED){
  int64_t i;
    for (i=0; i<19999999; i++){
    
        
    }
  thread_set_priority(35);
  for (i=0; i<59999999; i++){
    
        
    }
}




static void
block_thread (void *aux UNUSED) 
{
  

  msg ("Block thread acquiring lock...");
  lock_acquire (&candado);

  msg ("%s ...got it.", thread_name());

  int64_t i;
  
    for (i=0; i<69999999; i++){
    
        
    }
    lock_release(&candado);
}
