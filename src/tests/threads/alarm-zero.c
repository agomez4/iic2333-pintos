/* Tests timer_sleep(0), which should return immediately. */

#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"


static thread_func alarm_zero;
static thread_func cambio_prioridad;
static thread_func bloqueo;
static thread_func block_thread;
static int64_t wake_time;
static struct semaphore wait_sema;
struct lock candado;


void
test_alarm_zero (void) 
{
    
  lock_init(&candado);
  thread_create("meneh", 40, alarm_zero, NULL);
  thread_create("vip", 61, cambio_prioridad, NULL);
  thread_create("agugo", 55, alarm_zero, NULL);
  thread_create("kivon", 5, block_thread, NULL);
  thread_create("champa", 12, block_thread, NULL);
  thread_create("gulacre", 19, alarm_zero, NULL);
  thread_create("blito", 60, alarm_zero, NULL);
  thread_create("nico", 19, alarm_zero, NULL);
  
  timer_sleep (1000);
  pass ();

}

static void
alarm_zero (void *aux UNUSED)
{
    
    int64_t i;
    for (i=0; i<69999999; i++){
    
        
    }
}

static void
cambio_prioridad(void *aux UNUSED){
  int64_t i;
    for (i=0; i<19999999; i++){
    
        
    }
  thread_set_priority(1);
  for (i=0; i<59999999; i++){
    
        
    }
}




static void
block_thread (void *aux UNUSED) 
{
  

  msg ("Block thread acquiring lock...");
  lock_acquire (&candado);

  msg ("...got it.");

  int64_t i;
  
    for (i=0; i<69999999; i++){
    
        
    }
    lock_release(&candado);
}
