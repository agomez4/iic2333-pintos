/* Verifies that lowering a thread's priority so that it is no
   longer the highest-priority thread in the system causes it to
   yield immediately. */
#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"


static thread_func simp;
static thread_func cambio_prioridad;
static thread_func bloqueo;
static thread_func block_thread;
static int64_t wake_time;
static struct semaphore wait_sema;
struct lock candado;


void
test_priority_change (void) 
{
    
  lock_init(&candado);
  printf("se crean 4 threads uno de los cuales modifica su prioridad mientras se ejecuta\n");
  timer_sleep(100);
  thread_create("charmander", 40, simp, NULL);
  thread_create("squirtle", 61, cambio_prioridad, NULL);
  thread_create("bulbasaur", 55, simp, NULL);
  thread_create("pikachu",30,simp, NULL);
  
  timer_sleep (1000);
  pass ();

}

static void
simp (void *aux UNUSED)
{
    
    int64_t i;
    for (i=0; i<89999999; i++){
    
        
    }
}

static void
cambio_prioridad(void *aux UNUSED){
  int64_t i;
    for (i=0; i<39999999; i++){
    
        
    }
  thread_set_priority(35);
  msg("squirtle bajo su prioridad");

  for (i=0; i<59999999; i++){
    
        
    }
}




static void
block_thread (void *aux UNUSED) 
{
  

  msg ("Block thread acquiring lock...");
  lock_acquire (&candado);

  msg ("...got it.");

  int64_t i;
  
    for (i=0; i<69999999; i++){
    
        
    }
    lock_release(&candado);
}
